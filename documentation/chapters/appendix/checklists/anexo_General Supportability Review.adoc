=== Checklist General Supportability Review


====
.Checklist General Supportability Review
//[width="100%", cols="^1,^1,6,16", frame="topbot",options="header"]
[width="100%", cols="^1,^1,6,16", frame="topbot",options="header"]
|======================
|#       
|  
| Item    
| Comments


| 1
| image:warn.png[]
| The installation must be covered by active RH subscriptions to be supported.
| It must be completed after the analysis work on data collected


| 2
| image:warn.png[]
| A shared network must exist between the masters and the nodes.
| It must be completed after the analysis work on data collected


| 3
| image:warn.png[]
| Security-Enhanced Linux (SELinux) must be enabled on all of the cluster members.
| It must be completed after the analysis work on data collected


| 4
| image:warn.png[]
| Each host in the cluster must be configured to resolve hostnames from your DNS server.
| It must be completed after the analysis work on data collected


| 5
| image:warn.png[]
| NetworkManager is required on the nodes in order to populate dnsmasq with the DNS IP addresses.
| It must be completed after the analysis work on data collected


| 6
| image:warn.png[]
| Proxy must allow the communication between the cluster nodes and masters.
| It must be completed after the analysis work on data collected


| 7
| image:warn.png[]
| Time synchronization \| NTP
| It must be completed after the analysis work on data collected


| 8
| image:warn.png[]
| Cluster operators review
| It must be completed after the analysis work on data collected


|======================
====
